import React , { Component } from 'react';
import { StyleSheet, Text, View ,ScrollView,Image,TouchableOpacity,ListView,Alert,Dimensions,Platform} from 'react-native';
import GLOBAL from '../../constants/constants';
import { StackNavigator, DrawerActions } from 'react-navigation';
import { List, ListItem } from 'react-native-elements';
import MapView from 'react-native-maps';


class Maps extends Component {
    
  static navigationOptions = {
  }
  constructor(){
    super();
     
     console.log(Platform);
  //    this.state = {
  //     orientation: Platform.isPortrait() ? 'portrait' : 'landscape',
  //     devicetype: Platform.isTablet() ? 'tablet' : 'phone'
  // };

  // Event Listener for orientation changes
  // Dimensions.addEventListener('change', () => {
  //     this.setState({
  //         orientation: Platform.isPortrait() ? 'portrait' : 'landscape'
  //     });
  // });
  }
  
  
  render() {
      const { navigate } =this.props.navigation;
      const { region } = this.props;
      let { width, height } = Dimensions.get('window');
      return (
        <View style={styles.container}>
               <View style={{padding:30,height:60, backgroundColor:GLOBAL.COLOR.BRIGHT_PINK}}>
                    <TouchableOpacity   onPress={() => 
      navigate('DrawerOpen')} style={{width: 25, height: 25}}>
                    <Image source={require('../../images/menu.png')}  style={{width: 25, height: 25}} />
                    </TouchableOpacity>
                </View>
               <View>
                   <Text>
                   Maps {height}

                   </Text>
                   <View style ={{height:height,width:width}}  >
        <MapView
          style={styles.map}
          region={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}
        >
        </MapView>
      </View>
               </View>
        </View>
      );
    }
  }


const styles = StyleSheet.create({
    container: {
        flex: 1
      },  icon: {
            width: 24,
            height: 24,
          },
          containermap: {
            ...StyleSheet.absoluteFillObject,
           
            justifyContent: 'flex-end',
            alignItems: 'center',
          },
          map: {
            ...StyleSheet.absoluteFillObject,
          },
  });

  export default Maps;