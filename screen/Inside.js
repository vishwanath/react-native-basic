import React , { Component } from 'react';
import { StyleSheet, Text, View ,ScrollView,Image} from 'react-native';
import { DrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation';


import  First  from './First';
import  Second  from './Second';
import  Settings  from './settings/Settings';
import  Sidemenu  from './common/SideMenu';
import  Maps  from './map/Maps';
import  PullToRefresh  from './pullToRefresh/PullToRefresh';




const DrawerNavigatorApp = DrawerNavigator({
  First: { screen: First },
  Second: { screen: Second },
  PullToRefresh :{screen:PullToRefresh},
  Maps: { screen: Maps },
  Settings: { screen: Settings }
},{
  drawerPosition:'left',
  contentComponent:Sidemenu
  });


class Inside extends Component {
  
    render() {
      return (
          <DrawerNavigatorApp />
      );
    }
  }

  
  export default Inside;