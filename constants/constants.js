module.exports = {
    COLOR: {
      RED :'#ff1744',
      PINK:'#f50057',
      BRIGHT_PINK:'#ff1065',
      PURPLE:'#6a1b9a',
      DEEPPURLPLE:'#6200ea',
      INDIGO:'#3d5afe',
      BLUE:'#448aff',
      LIGHTBLUE:'#40c4ff',
      DARKBLUE: '#0F3274',
      CYAN:'#00e5ff',
      TEAL:'#00796b',
      GREEN:'#1b5e20',
      LIGHTGREEN:'#8bc34a',
      LIME:'#8bc34a',
      YELLOW:'#ffea00',
      UMBER:'#ffc400',
      ORANGE:'#ff9100',
      DEEPORANGE:'#ff3d00',
      BROWN:'#4e342e',
      GREY:'#616161',
      DARKGRAY: '#37474f',
      OFFWHITE:'#fbf7f5',
      WHITE:'#ffffff'
    }
  };
  