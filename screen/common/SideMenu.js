import PropTypes from 'prop-types';
import React, {Component} from 'react';
import styles from './SideMenu.style';
import { NavigationActions } from 'react-navigation';
import {ScrollView, Text, View} from 'react-native';
import { Avatar }  from 'react-native-elements';


class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
     
  }

  
  render () {
    const user ={
      name: 'brynn',
      avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
   };
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.userHeader}>

          <Avatar
          large
          rounded
  source={{uri: user.avatar}}
  
/>
        <Text style={{padding:10,fontSize:20,color:'#ffffff'}}>{user.name}</Text>
          </View>
          <View>
            <Text style={styles.sectionHeadingStyle}>
              Components
            </Text>
            <View style={styles.navSectionStyle}>
            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('PullToRefresh')}>
                Refresh
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Maps')}>
                Maps
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Settings')}>
              Settings
              </Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.footerContainer}>
          <Text>V 0.1</Text>
        </View>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;