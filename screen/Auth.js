import React , { Component } from 'react';
import { StyleSheet, Text,View  ,ScrollView,Image,KeyboardAvoidingView} from 'react-native';
import GLOBAL from '../constants/constants';
import { StackNavigator } from 'react-navigation';
import  Login  from './Login';
import  Inside  from './Inside';


const SimpleApp = StackNavigator({
  Login: { screen: Login, headerMode: 'none',
    header: null,
    navigationOptions: {
        header: null
    }  },
  Inside: { screen: Inside, headerMode: 'none',
  header: null,
  navigationOptions: {
      header: null,
      headerLeft: null
  }  
}
});



class Auth extends Component {
    render() {
      return (
       
        <SimpleApp/>
       
      );
    }
  }
  
  // <Image resizeMode="contain" style={styles.logo} source={require('../images/heart.png')} />
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: GLOBAL.COLOR.OFFWHITE
  }
});
  

export default Auth;