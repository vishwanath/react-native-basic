import React , { Component,AsyncStorage } from 'react';
import { StyleSheet, Text, View ,ScrollView,Image,TouchableOpacity,TextInput,Alert,KeyboardAvoidingView,ImageBackground} from 'react-native';
import GLOBAL from '../constants/constants';
import ApiUtils from '../Utils/APIUtils';
import  Inside  from './Inside';
import { StackNavigator} from 'react-navigation';



class Login extends Component {
    
   
    constructor() {
        super();
        this.state = {
            email: 'narsingh@tomar.com',
            password: '',
            error: ''
        }
    }

    componentDidMount() {
    // ApiUtils.getMoviesFromApi()
    //   .then((response) => response.json())
    //   .then((responseJson) => {

    //     console.log(responseJson.movies);

    //   })
    //   .catch((error) =>{
    //     console.error(error);
    //   });
    }

    onLoginPress() {
        x = this.state.password;
        this.props.navigation.navigate('Inside');

    //Inside
    }
      
       


    render() {
        
      return (
        <ImageBackground source={require('../images/login.jpg')} style={{width: '100%', height: '100%',flex: 1}} > 
        <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View  style={styles.container}>
            <View style={styles.logoContainer}>
                 <Text style={styles.logoText }>Logo</Text>
            </View>
            <View style={styles.loginContainer}>
               
                <TextInput style={ styles.input} value={this.state.email} autoCapitalize="none"  onSubmitEditing={()=> this.passwordInput.focus()} autoCorrect={false} keyboardType='email-address' returnKeyType="next" placeholder='Email' placeholderTextColor={GLOBAL.COLOR.BRIGHT_PINK} onChangeText={(text) => this.setState({email:text})} />
                <TextInput style={ styles.input} returnKeyType="go" ref={(input)=> this.passwordInput = input} placeholder='Password' placeholderTextColor={GLOBAL.COLOR.BRIGHT_PINK} secureTextEntry onChangeText={(text) => this.setState({password:text})} />
                <TouchableOpacity style={styles.buttonContainer} onPress={this.onLoginPress.bind(this)}>
                <Text style={styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>
            </View>
        </View>
        </KeyboardAvoidingView>
        </ImageBackground> 
      );
    }
  }


const styles = StyleSheet.create({
        container: {
            flex:1,
            alignItems: 'center'
        },
        logoContainer: {
            padding:100,
            flex:1,
            alignItems: 'center'
        },    
        loginContainer:{
            flex:2,
            alignItems: 'center',
            justifyContent: 'center'
        },
        buttonContainer:{
            backgroundColor: GLOBAL.COLOR.BRIGHT_PINK,
        },
       input:{
           height: 40,
           marginBottom: 10,
           padding: 10,
           width:300,
           backgroundColor:  GLOBAL.COLOR.WHITE,
           color: GLOBAL.COLOR.BRIGHT_PINK
       },
       logoText:{
            color : GLOBAL.COLOR.WHITE,
            fontWeight: '700',
            fontSize: 50
        },
       buttonText:{
           color: GLOBAL.COLOR.WHITE,
           textAlign: 'center',
           fontWeight: '700',
           width:300,
           height: 40,
           padding: 10
       }
  });

  export default Login;