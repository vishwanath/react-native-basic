export default {
    container: {
      paddingTop: 20,
      flex: 1
    },
    userHeader:{
      paddingBottom: 50,
      paddingTop:40,
      backgroundColor: '#ff1065',
      justifyContent: 'center',
      alignItems: 'center'
    },
    navItemStyle: {
      padding: 10
    },
    navSectionStyle: {
      backgroundColor: '#FFECF0'
    },
    sectionHeadingStyle: {
      paddingVertical: 10,
      paddingHorizontal: 5
    },
    footerContainer: {
      padding: 20,
      backgroundColor: '#FFECF0'
    }
  };