import {createSelector} from 'reselect';
import * as userSelectors from './price/selectors';

const user_id = state => state.user_id;
export const user_id = createSelector(user_id, user_id);
export const loading = createSelector(user_id, userSelectors.loading);