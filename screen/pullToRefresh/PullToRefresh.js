import React , { Component } from 'react';
import { StyleSheet, Text, View ,ScrollView,Image,TouchableOpacity,ListView,Alert,Dimensions} from 'react-native';
import GLOBAL from '../../constants/constants';
import { StackNavigator,DrawerActions} from 'react-navigation';
import { List, ListItem } from 'react-native-elements';
import  PTRView from 'react-native-pull-to-refresh';


class PullToRefresh extends Component {
    
  static navigationOptions = {
  }
  constructor(){
    super(); 
    this.state = {
        cards: [1, 2, 3]
      }
      this._refresh = this._refresh.bind(this);
  }

  _refresh () {
    // you must return Promise everytime
    return new Promise((resolve) => {
      setTimeout(()=>{
        // some refresh process should come here
        this.setState({cards: this.state.cards.concat([this.state.cards.length + 1])})
        resolve(); 
      }, 2000)
    })
  }


  
  render() {
      const { navigate } =this.props.navigation;
  
      let {width, height} = Dimensions.get('window');
      return (
        <View style={styles.container}>
                <View style={{padding:30,height:60, backgroundColor:GLOBAL.COLOR.BRIGHT_PINK}}>
                    <TouchableOpacity   onPress={() => navigate('DrawerOpen')} style={{width: 25, height: 25}}>
                            <Image source={require('../../images/menu.png')}  style={{width: 25, height: 25}} />
                    </TouchableOpacity>
                </View>
                <View>
                      
                        <View style ={{height:height,width:width}}>
                        <PTRView
                        style={{backgroundColor:'#F5FCFF'}}
                        onRefresh={this._refresh}
                      >
                        <View style={styles.container}>
                          <Text style={styles.welcome}> Pull Me!😸 </Text>
                          {this.state.cards.map((el, i) => (
                            <View style={styles.card} key={i}>
                              <Text style={styles.card__text}>Card: {el}</Text>
                            </View>
                          ))}
                        </View>
                      </PTRView>
                        </View>
               </View>
        </View>
      );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
      },
      header: {
        height: 60,
        borderColor: '#f9f9f9',
        backgroundColor: '#2196F3',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      headerText: {
        color: '#fff',
        fontSize: 20,
        lineHeight: 40,
      },
      card: {
        flex: 1,
        borderColor: '#fafafa',
        backgroundColor: '#2196F3',
        borderWidth: 2,
        borderRadius: 3,
        margin: 5,
      },
      card__text: {
        color: '#fafafa',
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      }
  });



  export default PullToRefresh;