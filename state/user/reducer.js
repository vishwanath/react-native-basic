import {loop} from 'redux-loop';
import {handleActions} from 'redux-actions';
import {USER_ACTIONS} from './types';
import {loginRequest} from './cmds';

const initialState = {
  user_id: null,
  error: null,
  loading: false
};

export default handleActions({
  [USER_ACTIONS.REQUEST]: state => loop(
    {user_id: null, loading: true, error: null},
    loginRequest
  ),

  [USER_ACTIONS.RESPONSE]: {
    next: (state, {payload}) =>
      ({loading: false, user_id: payload, error: null}),
    throw: (state, {payload}) =>
      ({loading: false, user_id: null, error: payload})
  }
}, initialState);
