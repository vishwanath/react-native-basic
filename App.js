import React , { Component }  from 'react';
import { StyleSheet, Text, View,SafeAreaView } from 'react-native';
import { TabNavigator,DrawerNavigator} from 'react-navigation';

import  Intro  from './screen/Intro';
import  Auth  from './screen/Auth';


export default class App extends Component{

  constructor(){
    super();
  }


  
  
  render() {
    // const App = TabNavigator({
    //   Intro: { screen: Intro },
    //   Auth: { screen: Auth },
    // },{
    //   navigationOptions: {
    //     tabBarVisible: false,
    //     swipeEnabled:true
    //   },
     
    // });

   
    return (
     
        
          <Auth style={styles.container}/>
       
     
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  safeArea: {
    flex: 1,
    backgroundColor: '#fff'
  }
});
