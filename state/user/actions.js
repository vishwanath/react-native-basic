import {createAction} from 'redux-actions';
import {USER_ACTIONS} from '../types';

export const loginRequest = createAction(USER_ACTIONS.LOGIN, () => null);
export const loginResponse = createAction(
    USER_ACTIONS.LOGIN_RESPONSE, ({ok, data}) =>
    ok ? data[0] : new Error('Couldnt fectch data')
);