
import {Cmd} from 'redux-loop';
import {loginResponse} from './actions';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

const request = async () => {
  const response = await global.fetch(
    'https://api.coinmarketcap.com/v1/ticker/bitcoin', {
      headers,
      method: 'GET'
    }
  );

  return {
    ok: response.ok,
    status: response.status,
    data: await response.json()
  };
};

export const loginRequest =
  Cmd.run(
    request, {
      successActionCreator: loginResponse
    }
  );