import React , { Component } from 'react';
import { StyleSheet, Text, View ,ScrollView,Image,TouchableOpacity,ListView,Alert} from 'react-native';
import GLOBAL from '../constants/constants';
import { StackNavigator,DrawerActions} from 'react-navigation';
import { List, ListItem } from 'react-native-elements';
class First extends Component {
    
  static navigationOptions = {
  }

  


  constructor(){
    super();
    //this.onPressRightToSet = this.onPressRightToSet.bind(this); // <-- Not this function
    this.renderRow = this.renderRow.bind(this); // <-- This function!!
    
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows([{
        name: 'Vishu singh',
        avatar_url: 'https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-1/p320x320/29793054_10156237827932512_3932602773937272313_n.jpg?_nc_cat=0&oh=07fad96f7e7f9eb2eeae1666fb9f0e4d&oe=5B99CDCD',
        subtitle: 'Developer'
      },
      {
        name: 'Amit Sharma',
        avatar_url: 'https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-1/p320x320/29176793_1873832662635929_2461649579305598976_n.jpg?_nc_cat=0&oh=04b7a4307b8e8e86310c871f38f75cc2&oe=5B858B63',
        subtitle: 'Tester'
      },
      {
        name: 'Sourabh mahana',
        avatar_url: 'https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-1/p320x320/27332715_1755539071133326_6288865952409240262_n.jpg?_nc_cat=0&oh=0414e60cb986f7d68d15116d6bcd0608&oe=5B7EFA1D',
        subtitle: 'iOS Dev'
      },
      {
        name: 'Sonal Sharma',
        avatar_url: 'https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-1/c97.0.320.320/p320x320/14095809_10154353935361425_4366390219469864823_n.jpg?_nc_cat=0&oh=2d1561e28bb28b34475ae35a5add8cf3&oe=5B90D7D6',
        subtitle: 'Tester'
      },
      {
        name: 'Mankirat Kaur',
        avatar_url: 'https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-1/p320x320/25446368_10159953464650195_6133980170515757820_n.jpg?_nc_cat=0&oh=af34e1cb7a19543f93e018a3aaa79ed8&oe=5B7DAF98',
        subtitle: 'Product manager'
      }]),
      userSelected:{
        name:"test",
        avatar_url: 'test',
        subtitle: 'test'
      }
    };
  }

  onPressRightToSet(rowdata){
    
                         //Toggle playing or stopped state with code that kind of looks like:
                         //this.setState({userSelected:rowData});
                        // this.props.navigation.navigate('Second', { ListViewClickItemHolder: rowData });
                        console.log("sdfsdfs");
                        console.log(JSON.stringify(rowdata));

                        Alert.alert(
                          'Alert Title',
                          rowdata.name+'is selected.',
                          [
                            {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                          ],
                          { cancelable: false }
                        )
  }

  renderRow (rowData, sectionID) {
   
    console.log("rowData----------------------------"+rowData);
    
    return (
      <ListItem
        roundAvatar
        key={sectionID}
        title={rowData.name}
        subtitle={rowData.subtitle}
        avatar={{uri:rowData.avatar_url}}
     
        onPressRightIcon={this.onPressRightToSet.bind(this,rowData)}
      />
    )
  }



    render() {
      const { navigate } =this.props.navigation;
      

      return (
        <View style={styles.container}>
               <View style={{padding:30,height:60, backgroundColor:GLOBAL.COLOR.BRIGHT_PINK}}>
                    <TouchableOpacity   onPress={() => 
      navigate('DrawerOpen')} style={{width: 25, height: 25}}>
                    <Image source={require('../images/menu.png')}  style={{width: 25, height: 25}} />
                    </TouchableOpacity>
                </View>
                <ScrollView>
                  <List>
                  <ListView
                  renderRow={this.renderRow}
                  dataSource={this.state.dataSource}
                  />
                  </List>
                </ScrollView>
        </View>
      );
    }
  }

const styles = StyleSheet.create({
    container: {
        flex: 1
      },
      
          icon: {
            width: 24,
            height: 24,
          }
  });

  export default First;